'''
Created on 2018年6月23日
从页面获取信息
@author: dark
'''
from urllib import parse,request
from getFromFile import getCookieFromFile
import json
import requests
import re

# 获取当前期数
def getStage():
    data={
        'Action': 'GetLotteryOpen',
        'LotteryCode': 1303,
        'IssueNo': 0,
        'DataNum': 10,
        'SourceName': 'PC'
        }
    headers = {
        'Connection': 'keep-alive',
        'Cookie': getCookieFromFile(),
        'Host': 'www.cpb02.com',
        'Origin': 'http://www.cpb02.com',
        'Referer': 'http://www.cpb02.com/lottery/PK10/1303',
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36'
        }
    url='http://www.cpb02.com/tools/ssc_ajax.ashx?A=GetLotteryOpen&S=cpb&U=q929609968'
#     对数据进行编码
    data = parse.urlencode(data).encode(encoding='utf-8')
#     构建请求并发送请求
    req = request.Request(url=url,data=data,headers=headers)
    try:
        res = request.urlopen(req, timeout=10)
        res = res.read()
        res = res.decode(encoding='utf-8')
        resJson = json.loads(res)
        return int(resJson['BackData'][0]['IssueNo'])+1
    except Exception as e:
        print(str(e))
        getStage()

# 获取当前安全冠军投注
def getSafePlan():
    headers = {
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Mobile Safari/537.36'
        }
    url='http://free.baibianjihua.com/jh/pk10/guanjun/5ma2qi.html'
#     构建请求并发送请求
    try:
        res = requests.get(url, headers=headers)
        res = res.content.decode(encoding='utf-8')
        res = re.search(str(getStage())+'(.*)……', res).group(0).split("┊")[2]
        res = re.search('(([0-9]+,)+[0-9]{2})', res).group(0).replace(',', ' ') + ',-,-,-,-,-,-,-,-,-'
        return res
    except Exception as e:
        print(str(e))
        getSafePlan()

















